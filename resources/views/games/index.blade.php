@extends('layouts.app')
@section('content')
    <div class="container">
        <br>
        @if(\Illuminate\Support\Facades\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Illuminate\Support\Facades\Session::get('success') }}</p>
            </div>
        @endif
        <div class="row">
            <div class="col text-center">
                <a href="{{ route('create-game') }}" class="btn btn-primary">Play Game</a>
            </div>
        </div>
    </div>
    @section('scripts')
        <script>
            $("document").ready(function(){
                setTimeout(function(){
                    $("div.alert").remove();
                }, 3000 );

            });
        </script>
    @endsection
@endsection
