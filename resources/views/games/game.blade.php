@extends('layouts.app')
@section('content')
    <div class="container">

        @if(\Illuminate\Support\Facades\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Illuminate\Support\Facades\Session::get('success') }}</p>
            </div>
        @endif
        <form action="{{ route('answer') }}" method="POST">
            @csrf
            <p>{{ $question->question }}</p>
            <div class="form-group">
                <label for="">Question</label>
                <input type="text" style="color: black" value="{{ $question->question }}" disabled class="form-control">
            </div>
            @foreach($question->answers as $key => $answer)
                <div class="form-check  ">
                    <input class="form-check-input" type="radio" value="{{ $answer->id }}" name="answer_id" id="id-{{ $key }}">
                    <label class="form-check-label" for="id-{{ $key }}">
                        {{ $answer->answer }}
                    </label>
                </div>
            <input type="hidden" name="question_id" value="{{ $question->id }}">
            <input type="hidden" name="game_id" value="{{ Request::route('game')->id }}">
            @endforeach
            <button type="submit" class="btn btn-primary">Send</button>
        </form>
    </div>

@section('scripts')
    <script>
        $("document").ready(function(){
            setTimeout(function(){
                $("div.alert").remove();
            }, 3000 );

        });
    </script>
@endsection

@endsection
