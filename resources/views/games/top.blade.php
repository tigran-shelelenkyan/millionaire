@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-9"></div>
            <div class="col-3">
                <a href="{{ route('play') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
        <br>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Full Name</th>
                <th scope="col">Point</th>
            </tr>
            </thead>
            <tbody>
            @foreach($topPlayers as $player)
            <tr>
                <th scope="row">1</th>
                <td>{{ $player['full_name'] }}</td>
                <td>{{ $player['point'] }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
