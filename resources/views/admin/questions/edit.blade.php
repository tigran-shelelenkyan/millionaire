@extends('layouts.admin')
@section('content')
    <h1 class="text-center">Edit Question</h1>
    <div class="container">
        <div class="m-auto">
            <form action="{{ route('question.update', ['question' => $questionAnswers->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <div class="col-12">
                                <label for="question-theme">Question theme</label>
                                <input type="text" name="theme" class="form-control" value="{{ $questionAnswers->theme }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="col-12">
                                <label for="point">Question point</label>
                                <input type="number" value="{{ $questionAnswers->point }}" name="point" min="5" max="20" class="form-control" id="point" aria-describedby="emailHelp">
                                <small id="emailHelp" class="form-text text-muted">Write question point!</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label for="question">Question</label>
                        <textarea class="form-control" name="question" id="question" rows="6">{{ $questionAnswers->question }}</textarea>
                    </div>
                </div>
                <div id="main-answer">
                    @foreach($questionAnswers->answers as $key => $answer)
                    <div class="answer-block" style="background-color: #f5f1f1; border-radius: 10px; margin-top: 10px">
                        <div class="form-group">
                            <div class="col-12">
                                <label for="point">Answer</label>
                                <input type="text" value="{{ $answer->answer }}" name="answers[{{ $key + 1 }}][answer]" class="form-control answerInput">
                            </div>
                        </div>
                        <div class="form-check">
                            <div class="col-12">
                                <input type="checkbox" name="answers[{{ $key + 1 }}][accuracy]" value="1" {{ $answer->accuracy ? 'checked' : '' }} class="form-check-input accuracy">
                                <label class="form-check-label" for="accuracy">Right or wrong!</label>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div id="answers" style="margin-top: 10px"></div>
                <div class="actions" style="margin-top: 10px">
                    <button class="btn btn-primary add-answer">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg>
                    </button>
                    <button class="btn btn-danger delete-answer">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-minus" viewBox="0 0 16 16">
                            <path d="M5.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                            <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z"/>
                        </svg>
                    </button>
                </div>
                <div style="margin-top: 20px">
                    <div class="col-1 float-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.add-answer').click(function (e) {
                e.preventDefault()
                console.log($('.answer-block'));
                let incrementNumber = $('.answer-block').length + 1

                let cloneElement = $('#main-answer .answer-block').first().clone()

                let accuracy = cloneElement.find('.accuracy');
                accuracy.prop('checked', false)
                accuracy.attr('name', 'answers['+incrementNumber+'][accuracy]')

                let answerInput = cloneElement.find('.answerInput');
                answerInput.val('').end().appendTo('#answers')
                answerInput.attr('name', 'answers['+incrementNumber+'][answer]')
            })

            $('.delete-answer').click(function (e) {
                e.preventDefault()
                if ($('.answer-block').length > 1) {
                    $('#main-answer .answer-block').last().remove()
                }
            })
        })
    </script>

@endsection
