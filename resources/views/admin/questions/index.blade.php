@extends('layouts.admin')

@section('content')
    <h1 class="text-center">Questions</h1>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-9"></div>
                <div class="col-3">
                    <a href="{{ route('question.create') }}" class="btn btn-primary">
                        Add Question
                    </a>
                </div>
            </div>
            <br>
            <div class="row">
                @forelse($questions as $question)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <div class="card-body">
                            <h5 class="card-title">{{ $question->theme }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Question point: {{ $question->point }}</h6>
                            <p class="card-text">{{ $question->question }}</p>
                            <a href="{{ route('question.edit', ['question' => $question->id]) }}" class="card-link">Edit</a>
                        </div>
                    </div>
                </div>
                @empty
                    <div class="alert alert-info m-auto" role="alert">
                        You don't have a questions in database !!!
                    </div>
                @endforelse
            </div>
        </div>

    </div>
@endsection
