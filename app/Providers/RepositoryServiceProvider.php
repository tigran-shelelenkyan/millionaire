<?php

namespace App\Providers;

use App\Repository\Eloquent\BaseRepository;
use App\Repository\GameRepository;
use App\Repository\Interfaces\EloquentRepositoryInterface;
use App\Repository\Interfaces\GameRepositoryInterface;
use App\Repository\Interfaces\QuestionRepositoryInterface;
use App\Repository\QuestionRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(QuestionRepositoryInterface::class, QuestionRepository::class);
        $this->app->bind(GameRepositoryInterface::class, GameRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
