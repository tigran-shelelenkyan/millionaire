<?php

namespace App\Repository\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface EloquentRepositoryInterface
{
    /**
     * @param int $id
     * @return Model|null
     */
    public function find(int $id) : ?Model;

    /**
     * @return Collection
     */
    public function findAll() : Collection;

    /**
     * @return Model
     */
    public function getModel() : Model;
}
