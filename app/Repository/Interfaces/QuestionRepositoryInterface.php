<?php

namespace App\Repository\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface QuestionRepositoryInterface
{
    /**
     * @param int $id
     * @return Model|null
     */
    public function getQuestionWithAnswers(int $id) : ?Model;

    /**
     * @return int
     */
    public function count() : int;
}
