<?php

namespace App\Repository\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface GameRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getGameHistory() : Collection;
}
