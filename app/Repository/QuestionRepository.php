<?php

namespace App\Repository;

use App\Models\Question;
use App\Repository\Eloquent\BaseRepository;
use App\Repository\Interfaces\QuestionRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class QuestionRepository extends BaseRepository implements QuestionRepositoryInterface
{
    /**
     * @param Question $model
     */
    public function __construct(Question $model)
    {
        parent::__construct($model);
    }

    /**
     * Get one question
     *
     * @param int $id
     * @return Model|null
     */
    public function getQuestionWithAnswers(int $id): ?Model
    {
        return $this->model->with('answers')->find($id);
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return $this->model->count();
    }
}
