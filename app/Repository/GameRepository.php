<?php

namespace App\Repository;

use App\Models\Game;
use App\Repository\Eloquent\BaseRepository;
use App\Repository\Interfaces\GameRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class GameRepository extends BaseRepository implements GameRepositoryInterface
{
    /**
     * @param Game $model
     */
    public function __construct(Game $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function getGameHistory() : Collection
    {
        return $this->model->selectRaw('game_histories.game_id AS gameId, game_histories.point, games.user_id')
            ->join('game_histories','games.id','=','game_histories.game_id')
            ->get();
    }
}
