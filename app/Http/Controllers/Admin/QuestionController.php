<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionRequest;
use App\Models\Question;
use App\Repository\Interfaces\QuestionRepositoryInterface;
use App\Repository\QuestionRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * @param QuestionRepository $questionRepository
     */
    public function __construct(QuestionRepositoryInterface $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $questions = $this->questionRepository->findAll();

        return view('admin.questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.questions.create');
    }

    /**
     * Store resource in storage.
     *
     * @param QuestionRequest $request
     * @return RedirectResponse
     */
    public function store(QuestionRequest $request): RedirectResponse
    {
        $data = $request->validated();

        /** @var Question $question */
        $question = $this->questionRepository->create($data);

        $question->answers()->createMany($data['answers']);

        $question->theme = Arr::get($data, 'theme');
        $question->question = Arr::get($data, 'question');
        $question->point = Arr::get($data, 'point');
        $question->save();

        return redirect()->route('question.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Question $question
     * @return Application|Factory|View
     */
    public function edit(Question $question)
    {
        return view('admin.questions.edit', ['questionAnswers' => $question]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param QuestionRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(QuestionRequest $request, int $id): RedirectResponse
    {
        $data = $request->validated();

        /** @var Question $question */
        $question = $this->questionRepository->find($id);
        $question->answers()->delete();
        $question->answers()->createMany($data['answers']);

        $question->theme = Arr::get($data, 'theme', $question->theme);
        $question->question = Arr::get($data, 'question', $question->question);
        $question->point = Arr::get($data, 'point', $question->point);
        $question->save();

        return redirect()
            ->route('question.index')
            ->with('success', 'Question successfully updated!');
    }
}
