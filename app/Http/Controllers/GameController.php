<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetQuestionRequest;
use App\Models\Answer;
use App\Models\Game;
use App\Models\GameHistory;
use App\Models\Question;
use App\Models\User;
use App\Repository\Interfaces\GameRepositoryInterface;
use App\Repository\Interfaces\QuestionRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    /**
     * @var GameRepositoryInterface
     */
    protected $gameRepository;

    /**
     * @var QuestionRepositoryInterface
     */
    protected $questionRepository;

    /**
     * @param GameRepositoryInterface $gameRepository
     * @param QuestionRepositoryInterface $questionRepository
     */
    public function __construct(GameRepositoryInterface $gameRepository, QuestionRepositoryInterface $questionRepository)
    {
        $this->gameRepository = $gameRepository;
        $this->questionRepository = $questionRepository;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('games.index');
    }

    public function getTopPlayers()
    {
        $groups = $this->gameRepository->getGameHistory()->groupBy('gameId');

        $topPlayers = $groups->map(function ($group) {
            return [
                'user_id' => $group->first()['user_id'],
                'full_name' => User::find($group->first()['user_id'])->full_name,
                'point' => $group->sum('point'),
            ];
        });

        $topPlayers = $topPlayers->sortByDesc('point')->unique('user_id');

        return view('games.top', compact('topPlayers'));
    }

    /**
     * @return RedirectResponse
     */
    public function createGame(): RedirectResponse
    {
        if ($this->questionRepository->count() < 5) {
            return redirect()
                ->back()
                ->with('warning', 'Count of questions not match!');
        }

        $game = Game::create([
            'user_id' => Auth::id()
        ]);

        return redirect()->route('game', ['game' => $game->id]);
    }

    /**
     * @param Game $game
     * @return Application|Factory|View|RedirectResponse
     */
    public function getQuestion(Game $game)
    {
        $questionData = $game->histories
            ->pluck('point', 'question_id')
            ->toArray();

        if (count($questionData) >= 5) {
            return redirect()
                ->route('play')
                ->with('success', 'Your game is over! your score is '.array_sum($questionData));
        }

        $question = Question::with('answers')
            ->inRandomOrder()
            ->whereNotIn('id', array_keys($questionData))
            ->first();

        return view('games.game', compact('question'));
    }

    /**
     * @param GetQuestionRequest $request
     * @return RedirectResponse
     */
    public function answer(GetQuestionRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $answer = Answer::find($data['answer_id']);
        $questionPoint = Question::find($data['question_id'])->point;
        $data['user_id'] = Auth::id();
        $data['point'] = $answer->accuracy ? $questionPoint : 0;

        GameHistory::create($data);

        return redirect()->back();
    }
}
