<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'theme' => 'required|string',
            'question' => 'required|min:5|max:10000',
            'point' => 'required|integer|between:5,20',
            'answers' => 'required|array|min:1',
            'answers.*.answer' => 'required|string|max:255',
            'answers.*.accuracy' => 'nullable',
        ];
    }
}
