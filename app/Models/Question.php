<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property $theme string
 * @property $question string
 * @property $point string
 */
class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'theme',
        'question',
        'point'
    ];

    /**
     * Get all answers for each question
     *
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany(Answer::class);
    }
}
