<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id'
    ];

    /**
     * @return HasMany
     */
    public function histories(): HasMany
    {
        return $this->hasMany(GameHistory::class,'game_id','id');
    }
}
