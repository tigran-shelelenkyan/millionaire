<?php

namespace Database\Seeders;

use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::create([
            'theme' => 'Football',
            'question' => 'Which team won the first Premier League title?',
            'point' => 15
        ])
            ->answers()
            ->createMany([
                [
                    'question_id' => 1,
                    'answer' => 'Manchester City',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 1,
                    'answer' => 'Manchester United',
                    'accuracy' => 1
                ],
                [
                    'question_id' => 1,
                    'answer' => 'Arsenal',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 1,
                    'answer' => 'Chelsea',
                    'accuracy' => 0
                ],
            ]);

        Question::create([
            'theme' => 'Football',
            'question' => 'With 202 clean sheets, which goalkeeper has the best record in the Premier League?',
            'point' => 18
        ])
            ->answers()
            ->createMany([
                [
                    'question_id' => 2,
                    'answer' => 'Petr Cech',
                    'accuracy' => 1
                ],
                [
                    'question_id' => 2,
                    'answer' => 'Viktor Valdes',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 2,
                    'answer' => 'Van Perci',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 2,
                    'answer' => 'van Der Sar',
                    'accuracy' => 0
                ],
            ]);

        Question::create([
            'theme' => 'Football',
            'question' => 'Which player scored the fastest hat-trick in the Premier League?',
            'point' => 14
        ])
            ->answers()
            ->createMany([
                [
                    'question_id' => 3,
                    'answer' => 'Andrey Arshavin',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 3,
                    'answer' => 'Van Perci',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 3,
                    'answer' => 'Fernando Torres',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 3,
                    'answer' => 'Sadio Mane',
                    'accuracy' => 1
                ],
            ]);

        Question::create([
            'theme' => 'Football',
            'question' => 'When was the inaugural Premier League season?',
            'point' => 20
        ])
            ->answers()
            ->createMany([
                [
                    'question_id' => 4,
                    'answer' => '1992-93',
                    'accuracy' => 1
                ],
                [
                    'question_id' => 4,
                    'answer' => '1994-95',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 4,
                    'answer' => '1991-92',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 4,
                    'answer' => '1995-96',
                    'accuracy' => 0
                ],
            ]);

        Question::create([
            'theme' => 'Football',
            'question' => 'How many clubs competed in the inaugural Premier League season?',
            'point' => 20
        ])
            ->answers()
            ->createMany([
                [
                    'question_id' => 5,
                    'answer' => '19',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 5,
                    'answer' => '20',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 5,
                    'answer' => '21',
                    'accuracy' => 0
                ],
                [
                    'question_id' => 5,
                    'answer' => '22',
                    'accuracy' => 1
                ],
            ]);
    }
}
