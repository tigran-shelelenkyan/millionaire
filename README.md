# Millionaire

## INSTALLATION
- create `.env` file
- copy content from `env.example` to `.env`
- configure `env` according to your settings
- run following commands
    ```shell
    $ composer install
    $ php artisan migrate --seed
    ```

## Admin Login and password
- admin@stdev.am
- qwerty123
