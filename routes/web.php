<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\QuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['admin', 'auth']], function () {
    Route::get('admin', [DashboardController::class, 'index'])->name('dashboard');
    Route::prefix('admin')->group(function () {
        Route::resource('question', QuestionController::class);
    });
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/game', [GameController::class, 'index'])->name('play');
    Route::get('/create-game', [GameController::class, 'createGame'])->name('create-game');
    Route::get('/game/{game}', [GameController::class, 'getQuestion'])->name('game');
    Route::post('/game/answer', [GameController::class, 'answer'])->name('answer');
    Route::get('/top-players', [GameController::class, 'getTopPlayers'])->name('top-players');
});
